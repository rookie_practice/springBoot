package com.springboot.springdemo;
//		import com.springboot.springdemo.Repository.UserRepository;
		import com.springboot.springdemo.Repository.StudenRepository;
		import com.springboot.springdemo.Repository.UserRepository;
		import com.springboot.springdemo.Repository.UserRepositoryMongodb;
		import com.springboot.springdemo.bean.Studen;
		import com.springboot.springdemo.bean.User;
		import org.hibernate.result.Output;
		import org.junit.Assert;
		import org.junit.Before;
		import org.junit.Test;
		import org.junit.runner.RunWith;
		import org.springframework.beans.factory.annotation.Autowired;
		import org.springframework.beans.factory.annotation.Qualifier;
		import org.springframework.boot.test.autoconfigure.data.redis.DataRedisTest;
		import org.springframework.boot.test.context.SpringBootTest;
		import org.springframework.data.redis.core.RedisTemplate;
		import org.springframework.jdbc.core.JdbcTemplate;
		import org.springframework.test.context.junit4.SpringRunner;
		import org.springframework.test.context.web.WebAppConfiguration;

		import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest             //单元测试
//@WebAppConfiguration      //测试环境
public class SpringdemoApplicationTests {
//
	@Autowired
	private UserRepositoryMongodb userRepository;

	@Test
	public void test() throws Exception {
//
		// 创建三个User，并验证User总数
		userRepository.save(new User(1L, "did", 30));
		userRepository.save(new User(2L, "mama", 40));
		userRepository.save(new User(3L, "kaka", 50));
		Assert.assertEquals(3, userRepository.findAll().size());

		// 删除一个User，再验证User总数
		User u = userRepository.findByName("did");
		userRepository.delete(u);
//		Assert.assertEquals(2, userRepository.findAll().size());
	}



//	@Autowired
//	private RedisTemplate<String, User> redisTemplate;
//
//	@Test
//	public void test() throws Exception {
//		// 保存对象
//		User user = new User("超人", 20);
//		redisTemplate.opsForValue().set(user.getName(), user);
//		user = new User("蝙蝠侠", 30);
//		redisTemplate.opsForValue().set(user.getName(), user);
//		user = new User("蜘蛛侠", 40);
//		redisTemplate.opsForValue().set(user.getName(), user);
//	}
//	@Autowired
//	private UserRepository userRepository;
//
//	@Autowired
//	private StudenRepository studenRepository;
//
//	@Test
//	public  void test()throws Exception {
//
//		userRepository.save(new User("eee", 12));
//		userRepository.save(new User("aaa", 13));
//		userRepository.save(new User("sss", 14));
//
//		studenRepository.save(new Studen("lili", 23));
//		studenRepository.save(new Studen("wangli", 11));
//		studenRepository.save(new Studen("liuhuan", 25));
//		studenRepository.save(new Studen("zahngsan", 15));
//	}
//	public void test() throws Exception {
//		// 创建10条记录
//		userRepository.save(new User("AAA", 10));
//		userRepository.save(new User("BBB", 20));
//
//
//	}

//	@Autowired
//	@Qualifier("primaryJdbcTemplate")
//	private JdbcTemplate jdbcTemplate1;
//
//	@Autowired
//	@Qualifier("secondaryJdbcTemplate")
//	private JdbcTemplate jdbcTemplate2;
//
//	@Test
//	public void test()throws Exception{
//		jdbcTemplate1.update("INSERT INTO USER (NAME ,age) VALUE ('李四',25)");
//		jdbcTemplate2.update("INSERT INTO USER (NAME ,age) VALUE ('赵四',29)");
//	}

//	@Autowired
//	private UserRepository userRepository;
//	@Test
//	public void test() throws Exception {
//		// 创建10条记录
////		userRepository.save(new User("III", 90));
//		userRepository.save(new User("110", 120));
//	}



//	private MockMvc mvc;
//	@Before
//	public void setUp() throws Exception {
//		mvc = MockMvcBuilders.standaloneSetup(
//				new HelloController(),
//				new UserController()).build();
//	}
//	@Test
//	public void getHello() throws Exception {
//		mvc.perform(MockMvcRequestBuilders.get("/hello").accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(equalTo("HelloWorld")));
//	}
//@Autowired
//	private UserService userService;
//	@Before
//	public void ready(){
//		userService.deleteAllUser();
//	}
//	@Test
//	public void addUser()throws Exception{
//		userService.create("lisi","1");
//		userService.create("zzzz","2");
//		userService.create("www","3");
//		userService.create("a","sd");
//		userService.create("e","ss");
//	}
//
//	@Test
//	public void updaa()throws Exception{
//	    userService.updataUserByName("e","e2e");
//    }



//	@Test
//	public void testUserController() throws Exception {
////  	测试UserController
//		RequestBuilder request = null;
//
////		// 1、get查一下user列表，应该为空
////		request = get("/users/");
////		mvc.perform(request)
////				.andExpect(status().isOk())
////				.andExpect(content().string(equalTo("[]")));
//
//		// 2、post提交一个user
//		request = post("/users/")
//				.param("id", "1")
//				.param("name", "测试大师")
//				.param("age", "20");
//		mvc.perform(request)
////				.andDo(MockMvcResultHandlers.print())
//				.andExpect(content().string(equalTo("success")));
////
////		// 3、get获取user列表，应该有刚才插入的数据
////		request = get("/users/");
////		mvc.perform(request)
////				.andExpect(status().isOk())
////				.andExpect(content().string(equalTo("[{\"id\":1,\"name\":\"测试大师\",\"age\":20}]")));
////
////		// 4、put修改id为1的user
////		request = put("/users/1")
////				.param("name", "测试终极大师")
////				.param("age", "30");
////		mvc.perform(request)
////				.andExpect(content().string(equalTo("success")));
////
////		// 5、get一个id为1的user
////		request = get("/users/1");
////		mvc.perform(request)
////				.andExpect(content().string(equalTo("{\"id\":1,\"name\":\"测试终极大师\",\"age\":30}")));
////
////		// 6、del删除id为1的user
////		request = delete("/users/1");
////		mvc.perform(request)
////				.andExpect(content().string(equalTo("success")));
////
////		// 7、get查一下user列表，应该为空
////		request = get("/users/");
////		mvc.perform(request)
////				.andExpect(status().isOk())
////				.andExpect(content().string(equalTo("[]")));
//	}
}


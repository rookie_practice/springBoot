package com.springboot.springdemo.Repository;

import com.springboot.springdemo.bean.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepositoryMongodb extends MongoRepository<User, Long> {
    User findByName(String name);
}

package com.springboot.springdemo.Repository;

import com.springboot.springdemo.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
//    User findByName(String name);
//    User findByNameAndAge(String name, Integer age);
//    @Query("select u from User u where u.name=:name")
//    User findUser(@Param("name") String name);

}

//public interface UserRepository extends CrudRepository<User, Long > {
//    User findByName(String name);
//    User findByNameAndAge(String name, Integer age);
//    @Query("select t from User t where t.userName=:name")
//    public User findUserByName(@Param("name") String name);
//}
package com.springboot.springdemo.Repository;

import com.springboot.springdemo.bean.Studen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudenRepository extends JpaRepository<Studen, Long> {

//    Studen findByName(String name);
//    Studen findByNameAndAge(String name, Integer age);
//    @Query("select u from Studen u where u.name=:name")
//    Studen findUser(@Param("name") String name);

}


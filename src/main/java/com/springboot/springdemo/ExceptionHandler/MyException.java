package com.springboot.springdemo.ExceptionHandler;
public class MyException extends Exception {
    public MyException(String message) {
        super(message);
    }
}

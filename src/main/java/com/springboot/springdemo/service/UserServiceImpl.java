package com.springboot.springdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.query.Param;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
//    @Autowired
//    private JdbcTemplate jdbcTemplate1;

    @Override
    public void create(String name, String password) {
//        jdbcTemplate1.update("insert into USER(NAME ,password) values(?, ?)", name, password);
    }

    @Override
    public void deleteByName(String name) {
//        jdbcTemplate1.update("DELETE from USER WHERE NAME =?", name);
    }

    @Override
    public void deleteAllUser() {
//        jdbcTemplate1.update("delete from USER");
    }

    @Override
    public void updataUserByName(String name, String newName) {
//        jdbcTemplate1.update("UPDATE user SET name=? WHERE name=?", newName, name);
    }
}

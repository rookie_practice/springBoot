package com.springboot.springdemo.service;

public interface UserService {
    /**
     * 创建一个用户
     * @param name    用户名
     * @param password   密码
     */
    void create(String name,String password);

    /**
     * 根据用户名删除用户
     * @param name
     */
    void deleteByName(String name);

    /**
     * 删除所有的用户
     */
    void deleteAllUser();

    /**
     * 更新用户的信息
     */
    void updataUserByName(String name,String newName);

}
